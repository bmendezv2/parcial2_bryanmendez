from django.db import models

# Create your models here.

class telefonos(models.Model):
    dni = models.CharField(max_length=0, unique=True)
    telefono = models.CharField(max_length=10, unique=True)

    class Meta:
        verbose_name = 'Telefono'
        verbose_name_plural = 'Telefonos'

    def __str__(self):
        return self.dni