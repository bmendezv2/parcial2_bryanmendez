from django.contrib import admin
from api.app.models import *

# Register your models here.
class telefonoadmin(admin.ModelAdmin):
    list_display = ['dni']

admin.site.register(telefonos, telefonoadmin)