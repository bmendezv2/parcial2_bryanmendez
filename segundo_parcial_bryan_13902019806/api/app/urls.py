from django.urls import path
from api.app.views import vistatelefono

app_name = 'app'

urlpatterns = [
    path('uno/', vistatelefono, name='vista1')
]