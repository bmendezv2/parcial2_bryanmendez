from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from api.app.models import telefonos


def vistatelefono(request):
    data = {
        'dni': '12345',
        'telefonos': telefonos.objects.all()
    }
    return render(request, 'home.html', data)